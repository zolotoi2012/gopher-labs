package main

import (
	"bufio"
	"encoding/json"
	"io"
	"log"
	"os"
	"strings"
)

type Message struct {
	Body string
}

const input = "GG"

func main() {
	Handle(os.Stdout)
}

func Handle(w io.Writer) {
	s := bufio.NewScanner(strings.NewReader(input))

	e := json.NewEncoder(w)
	for s.Scan() {
		m := Message{Body: s.Text()}
		err := e.Encode(m)
		if err != nil {
			log.Fatal(err)
		}

		log.Println(m)
	}

	if err := s.Err(); err != nil {
		log.Fatal(err)
	}
}
