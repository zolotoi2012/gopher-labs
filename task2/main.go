package main

import (
	"bufio"
	"encoding/json"
	"flag"
	"log"
	"net"
	"os"
)

var dialAddr = flag.String("dial", "localhost:8000", "host:port to dial")

type Message struct {
	Body string
}

func main() {
	Handle()
}

func Handle() {
	flag.Parse()

	s := bufio.NewScanner(os.Stdin)
	e := json.NewEncoder(os.Stdout)

	for s.Scan() {
		c := Connect()
		message := CreateMessage(s)
		err := e.Encode(message)
		c.Write([]byte(message.Body))
		if err != nil {
			return
		}

		if err != nil {
			log.Fatal(err)
		}
	}
	if err := s.Err(); err != nil {
		log.Fatal(err)
	}
}

func Connect() net.Conn {
	c, err := net.Dial("tcp", *dialAddr)
	if err != nil {
		log.Fatal(err)
	}

	return c
}

func CreateMessage(s *bufio.Scanner) Message {
	return Message{Body: s.Text()}
}
